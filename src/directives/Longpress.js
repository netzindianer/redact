/**
 * Longpress
 * v-longpress="300" - longpress time
 * @longpress emit when user hold down for min v-longpress time
 * @longclick emit when user clicked
 */
export default {
  bind: function (el, binding, vNode) {
    if (typeof binding.value !== 'number') {
      return
    }

    let pressTimer = null
    let longpressed = false

    let start = (e) => {
      e.preventDefault()
      e.stopPropagation()

      if (e.type === 'click' && e.button !== 0) {
        return
      }

      if (pressTimer === null) {
        pressTimer = setTimeout(() => {
          longpress()
        }, binding.value)
      }

      longpressed = false
    }

    let cancel = _ => {
      if (pressTimer !== null) {
        clearTimeout(pressTimer)
        pressTimer = null
      }
    }

    let click = _ => {
      _.preventDefault()
      _.stopPropagation()

      if (pressTimer !== null) {
        clearTimeout(pressTimer)
        pressTimer = null
      }

      if (longpressed) {
        return false
      }

      vNode.elm.dispatchEvent(new CustomEvent('longclick'))
    }

    let longpress = _ => {
      longpressed = true
      vNode.elm.dispatchEvent(new CustomEvent('longpress'))
    }

    el.addEventListener('mousedown', start)
    el.addEventListener('touchstart', start)
    el.addEventListener('click', click)
    el.addEventListener('mouseout', cancel)
    el.addEventListener('touchend', cancel)
    el.addEventListener('touchleave', cancel)
    el.addEventListener('touchcancel', cancel)
  }
}
