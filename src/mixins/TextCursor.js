/**
 * Mixin for manage cursor in
 * contentEditable component
 */
const TextCursor = ({
  data: () => ({
    textCursor: null
  }),
  methods: {
    /**
     * Set contenteditable element
     * @param {HTMLElement} input
     */
    setTextCursor (input) {
      this.textCursor = input
    },

    /**
     * Check if current cursor position is at start of element
     * @returns {Boolean} is on start
     */
    isCursorAtStart () {
      const selection = document.getSelection()

      if (selection.type !== 'Caret') {
        return
      }

      return [
        this._getFirstChildRecursive(),
        this.textCursor
      ].includes(selection.anchorNode) && selection.anchorOffset === 0
    },

    /**
     * Check if current cursor position is at end of element
     * @returns {Boolean} is on end
     */
    isCursorAtEnd () {
      const selection = document.getSelection()

      if (selection.type !== 'Caret') {
        return
      }

      const length = selection.anchorNode.length ? selection.anchorNode.length : selection.anchorNode.innerHTML.length
      return selection.anchorNode === this._getLastChildRecursive() && selection.anchorOffset === length
    },

    _getLastChildRecursive () {
      let el = this.textCursor

      while (el.lastChild !== null) {
        el = el.lastChild
      }

      return el
    },
    _getFirstChildRecursive () {
      let el = this.textCursor

      while (el && el.firstChild !== null) {
        el = el.firstChild
      }

      return el
    },

    /**
     * Move cursor to start
     */
    setCursorAtStart () {
      const el = this._getFirstChildRecursive()

      if (!el) {
        return
      }

      this._cleanCursor()

      const range = document.createRange()
      range.setStart(el, 0)

      this._setRange(range)
    },

    /**
     * Move cursor to end
     */
    setCursorAtEnd () {
      const el = this._getLastChildRecursive()

      if (!el) {
        return
      }

      this._cleanCursor()

      const range = document.createRange()
      const lastChild = this._getLastChildRecursive()
      range.setStart(lastChild, lastChild.length)

      this._setRange(range)
    },

    /**
     * Save state of current cursor
     */
    saveCursor () {
      const selection = document.getSelection()

      this._cleanCursor()

      if (!selection) {
        return
      }

      if (selection.type === 'Range') {
        this._saveRange(selection)
      }

      if (selection.type === 'Caret') {
        this._saveCaret(selection)
      }
    },
    _saveRange (selection) {
      let start = {node: selection.anchorNode, offset: selection.anchorOffset}
      let stop = {node: selection.focusNode, offset: selection.focusOffset}
      const position = start.node.compareDocumentPosition(stop.node)

      /* if selected from right to left => swap start with stop */
      if ((!position && selection.anchorOffset > selection.focusOffset) || position === Node.DOCUMENT_POSITION_PRECEDING) {
        [start, stop] = [stop, start]
      }

      let startRange = this._createCaretElement('start', start.offset)
      let stopRange = this._createCaretElement('stop', stop.offset)

      selection.anchorNode.before(startRange)
      selection.focusNode.after(stopRange)
    },
    _saveCaret (selection) {
      const caret = this._createCaretElement('caret', selection.anchorOffset)

      selection.anchorNode.before(caret)
    },
    _createCaretElement (name, offset) {
      const caretElement = document.createElement('span')
      caretElement.style.display = 'none'
      caretElement.setAttribute('text-cursor', name)
      caretElement.setAttribute('offset', offset)

      return caretElement
    },

    /**
     * Restore cursor state
     * @returns {Boolean} restore with success
     */
    restoreCursor () {
      const range = this._restoreCaret() ? this._restoreCaret() : this._restoreRange()

      range && this._setRange(range)

      return range !== null
    },
    _getElement (name) {
      return this.textCursor ? this.textCursor.querySelector(`span[text-cursor="${name}"]`) : null
    },
    _restoreRange () {
      const start = this._getElement('start')
      const stop = this._getElement('stop')

      if (!start && !stop) {
        return null
      }

      const range = document.createRange()
      range.setStart(start.nextSibling, start.getAttribute('offset'))
      range.setEnd(stop.previousSibling, stop.getAttribute('offset'))

      return range
    },
    _restoreCaret () {
      const caret = this._getElement('caret')

      if (!caret) {
        return null
      }

      const range = document.createRange()
      range.setStart(caret.nextSibling, caret.getAttribute('offset'))
      range.setEnd(caret.nextSibling, caret.getAttribute('offset'))

      return range
    },
    _cleanCursor () {
      ['start', 'stop', 'caret'].forEach(name => {
        const element = this._getElement(name)
        element && element.remove()
      })
    },
    _setRange (range) {
      const selection = document.getSelection()

      selection.removeAllRanges()
      selection.addRange(range)

      this._cleanCursor()
    },
    /**
     * Get clean text (without caret/cursor spans)
     * @returns {String} clean InnerHTML of element
     */
    cleanText (text) {
      const content = document.createElement('div')
      const textCursor = this.textCursor

      content.innerHTML = text
      this.textCursor = content
      this._cleanCursor()
      this.textCursor = textCursor

      return content.innerHTML
    }
  }
})

export default TextCursor
