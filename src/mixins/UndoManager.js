import cloneDeep from 'lodash.clonedeep'

export default options => ({
  // limit of max saved states. 0 = unlimited
  limit: 0,
  // actual index
  index: -1,
  // saved states
  states: [],
  //
  input: false,
  //
  events: {
    change: []
  },
  // merge with user options
  ...options,

  addStep (name) {
    this.states.splice(this.index + 1, this.states.length - this.index)

    this.states.push({
      name: name,
      state: cloneDeep(this.input())
    })

    if (this.limit && this.states.length > this.limit + 1) {
      this.states.splice(0, this.states.length - this.limit)
    }

    this.index = this.states.length - 1
  },

  clear () {
    this.index = -1
    this.states = []
  },

  canUndo () {
    return this.index > 0
  },

  canRedo () {
    return this.index < this.states.length - 1
  },

  getActionName (list) {
    return list.map(action => action.name)
  },

  undoList () {
    return this.getActionName(
      this.states.slice(1, this.index + 1)
    ).reverse()
  },

  undoLastAction () {
    return this.canUndo() ? this.undoList()[0] : ''
  },

  redoList () {
    return this.getActionName(
      this.states.slice(this.index + 1, this.states.length + 1)
    )
  },

  redoLastAction () {
    return this.canRedo() ? this.redoList()[0] : ''
  },

  undo (steps = 1) {
    if (this.canUndo()) {
      this.index -= steps

      this.emit('change', this.states[this.index].state)
    }
  },

  redo (steps = 1) {
    if (this.canRedo()) {
      this.index += steps

      this.emit('change', this.states[this.index].state)
    }
  },

  on (name, event) {
    !this.events[name] && (this.events[name] = [])

    this.events[name].push(event)
  },

  emit (name, data) {
    this.events[name] && this.events[name].forEach(event => event(data))
  }
})
