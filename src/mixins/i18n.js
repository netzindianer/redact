/**
 * I18N
 */

export default options => ({
  // default locale language
  lang: 'en',
  // register langs dictionaries:
  langs: {
    en: {}
  },
  // debug: show ###{$word} if word not exists in lang dictionary
  debug: false,
  // merge user options
  ...options,

  /**
   * Translate word to current locale language
   * @param {String} word word to translate
   * @param {Object} options variables to replace in template
   */
  translate (word, options = {}) {
    const lang = this.langs[this.lang]
    const msg = lang ? this._propertyFromPath(word, lang) : false

    if (!lang || !msg || typeof msg !== 'string') {
      if (this.debug) {
        console.warn(
          `Translation not found, or translation not return String! Word: "${word}", lang: ${this.lang}`,
          msg
        )
        return `###${word}`
      }

      return word
    }

    if (Object.keys(options).length === 0) {
      return msg
    }

    return Object
      .entries(options)
      .reduce((template, [name, value]) =>
        template.replace(`{${name}}`, value),
      msg
      )
  },

  /**
   * @returns {Array} available locales
   */
  locales () {
    return Object.keys(this.langs)
  },

  /**
   * @returns {String} current locale language
   */
  locale () {
    return this.lang
  },

  /**
   * Set current language
   * @param {String} lng new language
   */
  setLocale (lng) {
    this.lang = lng
  },

  _propertyFromPath (dotPath, obj) {
    return dotPath
      .split('.')
      .reduce((parts, name) => {
        parts[name] && (parts = parts[name])
        return parts
      }, obj)
  }
})
